//Контроллер страницы регистрации
angular.module('testApp').controller('signupCtrl',function($scope, $http, $state, $mdDialog){
    //Отправка заполненной формы
    $scope.signUp = function(){
        var url = 'http://test-trl/app/signup/signup.php';
        var data = {
            login: $scope.user.login,
            password: $scope.pass,
            email: $scope.user.email
        }

        $http.post(url,data)
            .then(function success(data){
                $mdDialog.show( //Модальное окно с информацией средствами ng-material
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#signup')))
                        .clickOutsideToClose(true)
                        .title('Поздравляем!')
                        .textContent('Регистрация прошла успешно! Теперь Вы можете войти на сайт с Вашими данными.')
                        .ok('OK')                          
                    ).then(function(){
                        console.log(data);
                        $state.go('login');
                        });
            },function error(data){//Обрабатываем возможные ошибки и сообщаем о них пользователю
                $mdDialog.show(
                    $mdDialog.alert()
                      .parent(angular.element(document.querySelector('#signup')))
                      .clickOutsideToClose(true)
                      .title('Ошибка при регистрации!')
                      .textContent(data.data.text)
                      .ok('OK')
                  );                
                console.log(data);
            });
    }

    $scope.goToLoginPage = function(){//Переход на страницу логина
        $state.go('login');
    }
})