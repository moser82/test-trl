<?php
header('Content-Type: application/json');
$data = [];
function save_to_db($login,$email,$hash){
    //Запись в БД нового пользователя
    try{
        $link = new PDO('mysql:host=localhost;dbname=trl','root',''); //Обращение к БД через PDO и подготовленные запросы - для безопасности
        $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //Проверим уникальность e-mail
        $sql_find = "
            SELECT * FROM users WHERE email = :email
        ";
        $stmt_find = $link->prepare($sql_find);
        $stmt_find->execute(array(
            ':email' => $email
        ));
        if (count($stmt_find->fetchAll())){
            return 'Пользователь с таким e-mail уже зарегистрирован';
        }
        //Проверим уникальность login
        $sql_find = "
            SELECT * FROM users WHERE login = :login
        ";
        $stmt_find = $link->prepare($sql_find);
        $stmt_find->execute(array(
            ':login' => $login
        ));
        if (count($stmt_find->fetchAll())){
            return 'Пользователь с таким логином уже зарегистрирован';
        }        
        $sql = "
            INSERT INTO users (login,email,hash) VALUES (:login, :email, :hash)
        ";
        $stmt = $link->prepare($sql);
        $stmt->execute(array(
            ':login' => $login,
            ':email' => $email,
            ':hash' => $hash
        ));
        return $stmt->rowCount();

    }catch (PDOException $e){
        return 'ОШИБКА '.$e->getMessage();
    }
}
$params = json_decode(trim(file_get_contents('php://input')), true);
if (isset($params['login']) && isset($params['email']) && isset($params['password'])){
    $login = ($params['login']);
    $email = ($params['email']);
    $pass = ($params['password']);
    //Проверим ограничение длины полей в БД
    if (strlen($login) > 128){
        $data = [
            'type' => 'error',
            'text' => 'Недопустимая длина логина'
        ];
        http_response_code(400);
        echo json_encode($data);
    }
    if (strlen($email) > 255){
        $data = [
            'type' => 'error',
            'text' => 'Недопустимая длина e-mail'
        ];
        http_response_code(400);
        echo json_encode($data);        
    }

    $hash = md5($pass.'$'.$login); //Кодируем поле hash
    $save_result = save_to_db($login,$email,$hash);
    
    if ($save_result){ //Обрабатываем возможные ошибки
        if ($save_result != 1){
            $data = [
                'type' => 'error',
                'text' => $save_result
            ];
            http_response_code(400);
        }else{
            $data = [
                'type' => 'success',
                'text' => 'Регистрация завершена успешно'
            ];            
        }
    }else{
        $data = [
            'type' => 'error',
            'text' => 'Регистрация завершилась неудачей. Обратитесь к администратору'
        ];
        http_response_code(400);
    }

    $json = json_encode($data);
    if ($json === false) {
        $json = json_encode(array("jsonError", json_last_error_msg()));
        if ($json === false) {
            $json = '{"jsonError": "unknown"}';
        }
        http_response_code(500);
    }
    echo $json;
}else{ //Данных не поступило
    http_response_code(400);
    echo json_encode("{'type': 'error', 'text': 'Недостаточно данных', 'data': ".$params['login']."}");
}
?>