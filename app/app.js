//Основной файл приложения

var testApp = angular.module('testApp',['ngMaterial', 'ngMessages', 'ui.router']);//Подключение дополнительных модулей

testApp.config(function ($stateProvider,$urlRouterProvider,$locationProvider){

    //Настройка роутинга приложения
    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state("login",{
            url: '/login',
            templateUrl: 'app/login/login.html',
            controller: 'loginCtrl'
        })
        .state("signup",{
            url: '/signup',
            templateUrl: 'app/signup/signup.html',
            controller: 'signupCtrl'
        })
        .state("profile",{
            url: '/profile',
            templateUrl: 'app/profile/profile.html',
            controller: 'profileCtrl'
        })

    //Убираем из адресной строки знак #
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: true
    });

})
