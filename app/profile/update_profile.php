<?php
header('Content-Type: application/json');
$data = [];
$p = json_decode(trim(file_get_contents('php://input')), true);

function update_user($hash, $full_name, $birthdate, $description){
    //Функция обновляет информацию в БД конкретного пользователя, найденного по закодированному полю hash
    try{
        $link = new PDO('mysql:host=localhost;dbname=trl','root','');
        $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql_update = "
            UPDATE users 
            SET
                full_name = :full_name,
                birthdate = :birthdate,
                description = :description
            WHERE hash = :hash
        ";
        $stmt_update = $link->prepare($sql_update);
        $stmt_update->execute(array(
            ':hash' => $hash,
            ':full_name' => $full_name,
            ':birthdate' => $birthdate,
            ':description' => $description
        ));
        
        if ($stmt_update->rowCount()){
            $data = array(
                'type' => 'success',
                'text' => 'Данные успешно обновлены'
            );
            return $data;
        }else{
            return 'Неверно указаны параметры доступа.';
        }

    }catch (PDOException $e){
        return 'ОШИБКА '.$e->getMessage();
    }
}

if (isset($p['hash']) && (isset($p['full_name']) || isset($p['birthdate']) || isset($p['description']))){
    $hash = ($p['hash']);
    $full_name = (isset($p['full_name']))? $p['full_name'] : null;
    $birthdate = (isset($p['birthdate']))? $p['birthdate'] : null;
    $description = (isset($p['description']))? $p['description'] : null;
    $result = update_user($hash, $full_name, $birthdate, $description);
    
    if ($result){           //Обрабатываем возможные ошибки
        if (!is_array($result)){
            $data = [
                'type' => 'error',
                'text' => $result
            ];
            http_response_code(400);
        }else{
            $data = $result;        
        }
    }else{
        $data = [
            'type' => 'error',
            'text' => 'Запрос к БД завершился неудачно. Обратитесь к администратору'
        ];
        http_response_code(500);
    }

    $json = json_encode($data);
    if ($json === false) {
        $json = json_encode(array("jsonError", json_last_error_msg()));
        if ($json === false) {
            $json = '{"jsonError": "unknown"}';
        }
        http_response_code(500);
    }
    echo $json;
}else{ //Данных не поступило
    http_response_code(400);
    echo json_encode("{'type': 'error', 'text': 'Недостаточно данных', 'data': ".$p['hash']."}");
}
?>