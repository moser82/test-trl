<?php
header('Content-Type: application/json'); //Определяем тип используемых данных
$data = [];
function find_user($hash){
    //Функция ищет пользователя по закодированному полю hash и возвращает объект user со всеми полями
    try{
        $link = new PDO('mysql:host=localhost;dbname=trl','root','');    //Безопасные соединения с БД
        $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql_find = "
            SELECT * FROM users WHERE hash = :hash LIMIT 1
        ";
        $stmt_find = $link->prepare($sql_find);
        $stmt_find->execute(array(
            ':hash' => $hash
        ));
        $result = $stmt_find->fetchAll();
        if (count($result)){
            foreach ($result as $user){
                $data = array(
                    'login' => $user['login'],
                    'email' => $user['email'],
                    'hash' => $user['hash'],
                    'full_name' => $user['full_name'],
                    'birthdate' => $user['birthdate'],
                    'description' => $user['description']
                );
            }
            return $data;
        }else{
            return 'Неверно указаны параметры доступа.';
        }

    }catch (PDOException $e){
        return 'ОШИБКА '.$e->getMessage();
    }
}
$params = json_decode(trim(file_get_contents('php://input')), true);
if (isset($params['hash'])){
    $hash = ($params['hash']);
    $result = find_user($hash);
    
    if ($result){           //Обрабатываем возможные ошибки
        if (!is_array($result)){
            $data = [
                'type' => 'error',
                'text' => $result
            ];
            http_response_code(400);
        }else{
            $data = $result;        
        }
    }else{
        $data = [
            'type' => 'error',
            'text' => 'Запрос к БД завершился неудачно. Обратитесь к администратору'
        ];
        http_response_code(500);
    }

    $json = json_encode($data);
    if ($json === false) {
        $json = json_encode(array("jsonError", json_last_error_msg()));
        if ($json === false) {
            $json = '{"jsonError": "unknown"}';
        }
        http_response_code(500);
    }
    echo $json;
}else{ //Данных не поступило
    http_response_code(400);
    echo json_encode("{'type': 'error', 'text': 'Недостаточно данных', 'data': ".$params['hash']."}");
}
?>