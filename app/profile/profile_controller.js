//Контроллер страницы профиля
angular.module('testApp').controller('profileCtrl',function($scope,$http,$state, $mdDialog){
    //Проверяем, авторизован ли пользователь
    var hash = localStorage.getItem('app'); 
    if (!(hash)){
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#profile')))
                .clickOutsideToClose(true)
                .title('Ошибка!')
                .textContent('Данная страница доступна только для авторизованных пользователей.')
                .ok('OK')                          
            ).then(function(){
                $state.go('login');
                });        
    }
    //Получаем данные пользователя
    var data = {
        hash: hash
    }
    $http.post('http://test-trl/app/profile/profile.php',data)
        .then(function success(data){
            $scope.user = data.data;
        },function error(data){ //Обрабатываем серверные ошибки
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#profile')))
                    .clickOutsideToClose(true)
                    .title('Ошибка!')
                    .textContent(data.data.text)
                    .ok('OK')                          
                ).then(function(){
                    console.log(data);
                    $state.go('login');
                    });
            console.log(data.status);
            console.log(data);
        });

    $scope.logOut = function(){ //Перейти на страницу входа
        localStorage.removeItem('app');
        $state.go('login');
    }

    $scope.updateProfile = function(){ //Обновление данных профиля
        var fullName = ($scope.user.full_name)? $scope.user.full_name : '';
        var birthDate = ($scope.user.birthdate)? $scope.user.birthdate : '';
        var description = ($scope.user.description)? $scope.user.description : '';
        var data = {
            hash: $scope.user.hash,
            full_name: fullName,
            birthdate: birthDate,
            description: description
        };
        $http.post('http://test-trl/app/profile/update_profile.php',data)
            .then(function success(data){ //Обновление прошло успешно - сообщим пользователю
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#profile')))
                        .clickOutsideToClose(true)
                        .title('Данные обновлены!')
                        .textContent(data.data.text)
                        .ok('OK')                          
                    ).then(function(){
                        console.log(data);
                        $state.go('profile');
                        });           
            },function error(data){ //Об ошибках - тоже сообщаем пользователю
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#profile')))
                        .clickOutsideToClose(true)
                        .title('Ошибка!')
                        .textContent(data.data.text)
                        .ok('OK')                          
                    ).then(function(){console.log(data);});
                console.log(data.status);
                console.log(data);                
            });
    }
})