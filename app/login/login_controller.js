//Контроллер формы входа на сайт
angular.module('testApp').controller('loginCtrl',function($scope, $http,$state, $mdDialog){
    $scope.login = function(){
        var url = 'http://test-trl/app/login/login.php';
        var data = {
            login: $scope.username,
            password: $scope.pass
        };

        $http.post(url,data)    //Поиск пользователя по указанным данным
            .then(function success(data){
                localStorage.setItem('app',data.data.hash); //Сохраняем hash пользователя для сохранения его сессии
                $state.go('profile');
                console.log(data);
            },function error(data){ //Ошибка поиска пользователя в БД
                $mdDialog.show( //Информационное окно средствами ng-Material
                    $mdDialog.alert()
                      .parent(angular.element(document.querySelector('#login')))
                      .clickOutsideToClose(true)
                      .title('Ошибка!')
                      .textContent(data.data.text)
                      .ok('OK')
                  );
                console.log(data);
            });
    }

    $scope.goToSignupPage = function(){ //Перейти на страницу регистрации
        $state.go('signup');
    }
})