<?php
header('Content-Type: application/json'); //Определяем тип входных данных
$data = [];
function find_user($hash){ //Функция производит поиск пользователя по БД по закодированному полю hash
    //Для обеспечения безопасности, запросы предварительно готовятся и отправляются средствами PDO
    try{
        $link = new PDO('mysql:host=localhost;dbname=trl','root','');
        $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql_find = "
            SELECT * FROM users WHERE hash = :hash LIMIT 1
        ";
        $stmt_find = $link->prepare($sql_find);
        $stmt_find->execute(array(
            ':hash' => $hash
        ));
        $result = $stmt_find->fetchAll();
        if (count($result)){
            foreach ($result as $user){
                $data = array(
                    'hash' => $user['hash']
                );
            }
            return $data;
        }else{
            return 'Неверно указаны логин и/или пароль.';
        }

    }catch (PDOException $e){
        return 'ОШИБКА '.$e->getMessage();
    }
}
$params = json_decode(trim(file_get_contents('php://input')), true); //Преобразование входных данных в массив
if (isset($params['login']) && isset($params['password'])){
    $login = ($params['login']);
    $pass = ($params['password']);

    $hash = md5($pass.'$'.$login); //Кодируем поле hash
    $result = find_user($hash);
    
    if ($result){
        if (!is_array($result)){ //Обрабатываем возможные ошибки
            $data = [
                'type' => 'error',
                'text' => $result
            ];
            http_response_code(400);
        }else{
            $data = $result;        
        }
    }else{          //Обрабатываем возможные ошибки
        $data = [
            'type' => 'error',
            'text' => 'Запрос к БД завершился неудачно. Обратитесь к администратору'
        ];
        http_response_code(500);
    }

    $json = json_encode($data);
    if ($json === false) {
        $json = json_encode(array("jsonError", json_last_error_msg()));
        if ($json === false) {
            $json = '{"jsonError": "unknown"}';
        }
        http_response_code(500);
    }
    echo $json;
}else{ //Данных не поступило
    http_response_code(400);
    echo json_encode("{'type': 'error', 'text': 'Недостаточно данных', 'data': ".$params['login']."}");
}
?>